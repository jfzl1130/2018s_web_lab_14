<%@ page import="ictgradschool.web.simplewebapp.inclassexample.CatArticle" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Slightly Less Tedious Cat Article Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>

<%@ include file="my-menu.jsp"%>

<%!

    public static List<CatArticle> getArticles() {

        List<CatArticle> articles = new ArrayList<>();

        articles.add(new CatArticle("Crazy Cheetah", "./images/cheetah.jpg", "Persian malkin british shorthair. Malkin. Panther russian blue siamese for siberian and mouser. Balinese malkin birman tiger and puma. Mouser russian blue for lion ocelot grimalkin. Bombay turkish angora and scottish fold maine coon havana brown american shorthair siamese. Siamese. Donskoy devonshire rex and thai munchkin, so scottish fold. Bombay persian. Burmese abyssinian."));
        articles.add(new CatArticle("Lovely Lion", "./images/lion.jpg", "Cheetah birman and panther, havana brown so bobcat or savannah but havana brown. Munchkin jaguar birman leopard british shorthair or bengal yet donskoy. Havana brown bengal mouser but ragdoll. Malkin siamese. Tiger thai siberian. Siamese ocelot birman. Norwegian forest bombay kitty tiger. Norwegian forest. Maine coon. Cougar kitty and singapura british shorthair, lion so birman. Munchkin. Birman mouser munchkin cougar cheetah and american shorthair."));
        articles.add(new CatArticle("Talented Tiger", "./images/tiger.jpg", "Kitten. Scottish fold. Bombay. Tiger siamese. Bobcat american bobtail egyptian mau russian blue american bobtail, for devonshire rex. British shorthair russian blue maine coon sphynx yet abyssinian and devonshire rex. Sphynx american bobtail but thai lion for havana brown, cougar yet devonshire rex. Donskoy puma so munchkin bobcat and malkin malkin turkish angora."));
        articles.add(new CatArticle("Fluffy Feline", "./images/feline.jpg", "Russian blue. Turkish angora bengal. Cougar egyptian mau yet devonshire rex siberian. Havana brown ragdoll scottish fold. American bobtail lynx donskoy. British shorthair birman bobcat and russian blue but cheetah cornish rex. Bengal puma. Russian blue bengal or cougar. Malkin thai."));
        articles.add(new CatArticle("Powerful Panther", "./images/panther.jpg", "Sphynx grimalkin. Bobcat. Singapura balinese egyptian mau burmese. Bobcat american shorthair birman russian blue and siberian. Cheetah. Puma bombay ocicat. Munchkin balinese balinese but thai. Manx."));
        articles.add(new CatArticle("Wonderful Wildcat", "./images/wildcat.jpg", "Tiger ragdoll or tiger jaguar but siberian bengal. Cougar birman bombay. Maine coon mouser, munchkin. Abyssinian persian. Lion havana brown."));

        return articles;

    }


%>

<!-- HTML comment, will appear in browser -->
<%-- JSP comment, will NOT appear in browser --%>
<div class="container mt-4">
    <div class="row">

        <%
            List<CatArticle> articles = getArticles();

            for (CatArticle article : articles) {

                %>

        <div class="col-lg-4 col-md-6 col-sm-12 mb-4">

            <div class="card h-100">
                <img class="card-img-top" src="<%=article.getImage()%>">
                <div class="card-body">
                    <h5 class="card-title"><%=article.getTitle()%></h5>
                    <p class="card-text"><%=article.getContent()%></p>
                </div>
            </div>

        </div>

        <%
            }
        %>

    </div>
</div>

</body>
</html>
