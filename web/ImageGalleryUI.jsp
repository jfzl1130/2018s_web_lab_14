<%@ page import="ictgradschool.ictgradschool.web.lab14.imagegallery.ImageGalleryDisplay" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.LinkedList" %><%--
  Created by IntelliJ IDEA.
  User: sliu048
  Date: 10/01/2019
  Time: 2:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <title></title>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

</head>
<body>

<p><%= request.getAttribute("photosFolder")%></p>





<%--<p><%--%>
    <%--// retrieve your list from the request, with casting--%>
    <%--LinkedList<ImageGalleryDisplay.FileInfo> list = (LinkedList<ImageGalleryDisplay.FileInfo>) request.getAttribute("fileDataList");--%>
<%--%></p>--%>



<table>

    <%--this is the sorting pic and function --%>

    <tr>
       <th>Image</th>

        <th><a href="/ImageGalleryDisplay?sortColumn=filename&order=${filenameSortToggle}ending">File Name

        <img src="images/sort-${filenameSortToggle}.png"></a></th>

        <th><a href="/ImageGalleryDisplay?sortColumn=filesize&order=${filesizeSortToggle}ending">File Size

            <img src="images/sort-${filesizeSortToggle}.png"></a></th>




<%-- ${fileDataList} key from servelet  --%>

    </tr>
    <c:forEach items="${fileDataList}" var="FileInfo">
        <tr>

            <td><img src="/Photos/${FileInfo.thumbPath.name}"></td>

            <td>${FileInfo.thumbDisplay}</td>

            <td>${FileInfo.fullfileSize}</td>




        </tr>
    </c:forEach>
</table>
</body>
</html>
